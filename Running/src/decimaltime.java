/* 
 * Copyright 2013 (C) Boye Inc. 
 *  
 * Created on : 02-04-13 
 * Author     : Mads Boye
 * Version	  : 0.1.3 
 */  
import java.nio.file.attribute.AclEntry.Builder;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;


public class DecimalTime {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*System.out.println("Pace: " + paceCalculator(1,57,30,21));
		System.out.println("Distance: " + distanceCalculator(1, 57, 0, 5, 33));*/
		//System.out.println("time: " + timeCalculator(3, 2, 30) + "\n");
		System.out.println(DecimalTime.class.getPackage().getImplementationVersion());
		System.out.println(runningprogram(2, 0, 0, 21, 6, 0, 30));
		//test();
	}

	public static double paceCalculator(int hours, int minutes, int seconds, double distance) {
		double decimalTime = decimalTimeConverter(hours, minutes, seconds);
		double pace = decimalTime/distance;
		return pace;
	}

	public static double distanceCalculator(int hours, int minutes, int seconds, int paceMintes, int paceSeconds) {
		double deciPaceSeconds = paceSeconds/60.0;
		double deciPace = paceMintes+deciPaceSeconds;
		double time = decimalTimeConverter(hours, minutes, seconds);
		double distance = time/deciPace;
		return distance;
	}

	public static double timeCalculator(double distance, int paceMintes, int paceSeconds) {
		double deciPace =decimalTimeConverter(0, paceMintes, paceSeconds);
		double time = distance * deciPace;
		return time;

	}
	
	public static void test() {
		int b = 5;
		for (int i = 0; i < b; i++) {
			System.out.println("hej"+i);
			if (i<b-1) {
				System.out.println("daw");
			} else {
				
			}
			}
	}

	//TODO change return type for HashMap<String,String> later to get proper output array. LinkedHashMap<String,String>
	public static LinkedHashMap<String,String> runningprogram(int hours, int minutes, int seconds, double distance, int depot, int minutesprdepot, int secondsprdepot) {
		double decimalDepotTime = decimalTimeConverter(0, minutesprdepot, secondsprdepot); //Depot time to decimal time.
		double depotTime = (decimalDepotTime)*depot; //Total depot time.
		double runningTime = decimalTimeConverter(hours, minutes, seconds)-depotTime; // Running time = wished time minus depot time.
		TimeConverter splitTime = timeConverter(runningTime);
		double splitPace = paceCalculator(splitTime.getHours(), splitTime.getMinutes(), splitTime.getSeconds(), distance);
		TimeConverter SplitPaceTime = timeConverter(splitPace);
		double distanceSplit = distance/(depot+1); //added 1 to compensate for the depots at the goal line.
		//TODO check that this piece of code is robust!
		LinkedHashMap<String, String> rp = new LinkedHashMap<String, String>();
		for (double i = distanceSplit; i <= distance; i+=distanceSplit) {
			TimeConverter splitTimeloop = timeConverter(timeCalculator(i, SplitPaceTime.getMinutes(), splitTime.getSeconds()));
			String testHours = Integer.toString(splitTimeloop.getHours());
			String testMiuntes = Integer.toString(splitTimeloop.getMinutes());
			String testSeconds = Integer.toString(splitTimeloop.getSeconds());
			rp.put(Double.toString(i),testHours+":"+testMiuntes+":"+testSeconds);
		}
		//FIXME
		LinkedHashMap<String, String> depotList = new LinkedHashMap<String, String>();
//		for (double j = 0; j < depot; j++) {
//			while(k.hasNext()) {
//				Map.Entry<String, String> me =(Entry<String, String>)k.next();
//				
//				System.out.print("Dist: " + me.getKey());
//				System.out.println(" Time: " +me.getValue());
//				depotList.put("Depot",testHours+":"+testMiuntes+":"+testSeconds);
//			}
//			TimeConverter splitTimeloop = timeConverter(timeCalculator(i, SplitPaceTime.getMinutes(), splitTime.getSeconds()));
//			String testHours = Integer.toString(splitTimeloop.getHours());
//			String testMiuntes = Integer.toString(splitTimeloop.getMinutes());
//			String testSeconds = Integer.toString(splitTimeloop.getSeconds());
////			depotList.put("Depot",testHours+":"+testMiuntes+":"+testSeconds);
//		}

		

				Set<Entry<String, String>> set = rp.entrySet();
				Iterator<Entry<String, String>> k = set.iterator();
//				while(k.hasNext()) {
//					Map.Entry<String, String> me =(Entry<String, String>)k.next();
//					
//					System.out.print("Dist: " + me.getKey());
//					System.out.println(" Time: " +me.getValue());
//				}
		return null;
	}

	public static double decimalTimeConverter(int hours, int minutes, int seconds){
		double deciSeconds = seconds/60.0;
		double totalTime = (hours*60)+minutes+deciSeconds;
		return totalTime;
	}

	public static TimeConverter timeConverter(double deciTime) {
		int hours = (int)deciTime/60;
		int minutes = (int)deciTime%60;
		double deciSeconds = (deciTime - (int)deciTime);
		int seconds = (int)(deciSeconds*60);
		//System.out.println(hours +" "+ minutes+" "+ deciSeconds+" "+ seconds);
		return new TimeConverter(hours, minutes, seconds);
	}

	public static void calories(String[] args){
		//Function for estimating used calories for a given run.
	}

	public static void formatter(String[] args){
		//DecimalFormat formatter = new DecimalFormat("00");
		//int hours = 0;
		//Must format the output of the pace, distance, and time method
		//TODO The below code must be moved to the formatter method.

		/* TODO DecimalFormat formatter = new DecimalFormat("00");
		String pace = Double.toString(time);
		double deciPace = time/distance;
		System.out.println(deciPace);
		int paceMinutes = (int)deciPace;
		double deciPaceSeconds = deciPace - (int)deciPace;
		int paceSeconds = (int)(Math.round(deciPaceSeconds *60));
		String intSeconds = formatter.format(paceSeconds);
		String pace = Integer.toString(paceMinutes) + ":" + intSeconds;*/
		// TODO The code below must be moved to the formatter method.
		//DecimalFormat formatter = new DecimalFormat("##.00");
		//formatter.setMaximumFractionDigits(2);
		/*BigDecimal bd = new BigDecimal(distanceUnrounded);
		BigDecimal distance = bd.setScale(2, BigDecimal.ROUND_HALF_EVEN);*/
		//double distance = Double.valueOf(formatter.format(distanceDecimals));
		//df.format(Double.valueOf(distance));
		/* TODO move to formatter method		System.out.println(deciTime);
		double deciTimeSeconds = deciTime - (int)deciTime;
		long longMinutes = Math.round(deciTime - deciTimeSeconds);
		int minutes = (int)longMinutes;
		System.out.println("Before if: " + minutes);
		int timeSeconds = (int)Math.round(deciTimeSeconds*60);
		if (minutes >= 60) {
			hours = minutes/60;
			minutes = minutes%60;
		}
		System.out.println(minutes);
		if (timeSeconds == 60) {
			minutes = minutes + 1;
			timeSeconds = 0;
		}
		String strSeconds = formatter.format(timeSeconds);
		System.out.println(strSeconds);
		String time = Integer.toString(hours) + ":" + Integer.toString(minutes) + ":" + strSeconds;*/
	}

}
