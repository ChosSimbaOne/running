public class TimeConverter {


	private int hours = 0;
	private int minutes = 0;
	private int seconds = 0;

	public int getHours() {
		return hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public int getSeconds() {
		return seconds;
	}


	public TimeConverter(int h, int m, int s){
		hours = h;
		minutes = m;
		seconds = s;
	}


}
